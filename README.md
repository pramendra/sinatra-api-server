# sinatra-api-server



## Requirements

- ruby

## Setup

### Run API Server

```
$ git clone git@github.com:pramendra/sinatra-api-server.git
$ cd sinatra-api-server.git
$ bundle install
$ bundle exec foreman start
```

#### _change log in Api server_

```
* please clone this version as apis were not working due to cross origin issue
* updated localhost to 0.0.0.0 to support docker
```

### 