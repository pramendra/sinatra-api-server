require 'sinatra'
require 'sinatra/json'
require 'json'

get '/items' do
  headers 'Access-Control-Allow-Origin' => '*'

  data = File.read('./json/all.json')
  response = JSON.load(data)
  json response, content_type: :js
end

get '/items/:id' do
  headers 'Access-Control-Allow-Origin' => '*'
  idx = params['id'].to_i - 1

  data = File.read('./json/all.json')
  response = JSON.load(data)

  json response['data'][idx], content_type: :js
end

get '/categories' do
  headers 'Access-Control-Allow-Origin' => '*'
  data = File.read('./json/categories.json')
  response = JSON.load(data)

  json response, content_type: :js
end

get '/not_found' do
  headers 'Access-Control-Allow-Origin' => '*'
  status 404
end
