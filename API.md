## Launching the API Server

```
$ cd api-server
$ bundle install
$ bundle exec foreman start
$ open http://localhost:5000
```

# API Document

## GET /items

### Response

```
{
  "data": [
    {
      "id": "1",
      "name": "men1",
      "description": "size free 1",
      "like_count": 91,
      "comment_count": 59,
      "price": 51,
      "isSoldOut": false,
      "shippingFee": "Free shipping",
      "image": "http://dummyimage.com/400x400/000/fff?text=men1"
    },
    {
      "id": "2",
      "name": "men2",
      "description": "size free 1",
      "like_count": 0,
      "comment_count": 0,
      "price": 10000,
      "isSoldOut": true,
      "shippingFee": "Free shipping",
      "image": "http://dummyimage.com/400x400/000/fff?text=men2"
    },
    ...
  ]
}
```

## GET /items/:id

### Response

```
{
  {
    "id": "1",
    "name": "men1",
    "description": "size free 1",
    "like_count": 91,
    "comment_count": 59,
    "price": 51,
    "isSoldOut": false,
    "shippingFee": "Free shipping",
    "image": "http://dummyimage.com/400x400/000/fff?text=men1"
  }
}
```

## GET /categories

### Response

```
{
  "data": [
    {
      "id": "1",
      "name": "All"
    },
    {
      "id": "2",
      "name": "Women"
    },
    {
      "id": "3",
      "name": "Kids"
    },
    {
      "id": "4",
      "name": "Electronics"
    },
    {
      "id": "5",
      "name": "Men"
    }
  ]
}
```
